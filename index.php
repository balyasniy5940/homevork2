<?php
$a = 42;
$b = 55;
echo $a < $b ? "$b больше $a<br>" : "$b меньше $a<br>";

$a = rand(5, 15);
$b = rand(5, 15);
echo $a < $b ? "$b больше $a<br>" : "$b меньше $a<br>";

$name = 'Oleg';
$surname = 'Balyasniy';
$patronymic = 'Nikolaevich';
echo "$surname {$name[0]}. {$patronymic[0]}.<br>";

$number = 7;
$randNumber = rand(1, 99999);
$countNum = substr_count($randNumber, $number);
echo "$number встречается $countNum раз в числе $randNumber<br>";

$a = 3;
echo "$a <br>";

$a = 10;
$b = 2;
echo "$a + $b = " . ($a + $b) . "<br>";
echo "$a - $b =" . ($a - $b) . " <br>";
echo "$a * $b =" . ($a * $b) . " <br>";
echo "$a % $b =" . ($a % $b) . " <br>";

$c = 15;
$d = 2;
$result = $c + $d;
echo $c . ' + ' . $d . ' = ' . $result . "<br>";

$a = 10;
$b = 2;
$c = 5;
echo "$a + $b + $c = " . ($a + $b + $c) . "<br>";

$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;
$result = $c + $d;
echo $result . "<br>";

$text = 'Привет, Мир!';
echo $text . "<br>";

$text1 = 'Привет, ';
$text2 = 'Мир!';
echo $text1 . $text2 . "<br>";

$minutesInHour = 60;
$secInHour = 60 * $minutesInHour;
$secInDay = 24 * $secInHour;
$secInWeek = 7 * $secInDay;
$secInMonth = 30 * $secInDay;
echo "Секунд в 1 часе: $secInHour Секунд в 1 сутках: $secInDay Секунд в 1 неделе: $secInWeek Секунд в 1 месяце : $secInMonth" . "<br>";

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
++$var;
--$var;
echo $var . "<br>";


$sec = date('s');
$minute = date('i');
$hour = date('h') + 3;
echo "The current time: $hour:$minute:$sec<br>";

$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text . "<br>";


$foo = 'bar';
$bar = 10;
echo $$foo;
echo "<br>";

$a = 2;
$b = 4;
echo $a++ + $b; // 6
echo $a + ++$b; // 8
echo ++$a + $b++; // 9
echo "<br>";

$a = 'Nix';
echo "<br>";
echo isset($a) ? 'переменная существует' : 'переменная не существует';

$a = 20;
echo "<br>";
echo gettype($a) == 'int' ? 'тип переменной int' : 'не int';

$a = null;
echo "<br>";
echo is_null($a) ? 'переменная со значением NULL' : 'переменной присвоено значение';

$a = 235;
echo "<br>";
echo is_integer($a) ? 'целое число' : 'не целое число';

$a = 19.45;
echo "<br>";
echo is_double($a) ? 'число с плавающей точкой' : 'не число с плавающей точкой';

$a = 'Name';
echo "<br>";
echo is_string($a) ? 'переменная строка' : 'переменная не строка';

$a = '856';
echo "<br>";
echo is_numeric($a) ? 'переменная число' : 'переменная не число';

$a = false;
echo "<br>";
echo is_bool($a) ? 'булевая переменная' : 'не булевая переменная';

$a = true;
echo "<br>";
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 45.56778;
echo "<br>";
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 'abc';
echo "<br>";
echo is_null($a) ? 'переменная NULL' : 'переменная не NULL';

$a = [1, 2, 3];
echo "<br>";
echo is_array($a) ? 'переменная массив' : 'переменная не массив';

$a = (object)'Nix';
echo "<br>";
echo is_object($a) ? 'переменная объект' : 'переменная не объект';
echo "<br>";
$a = 123;
$b = 234;
echo $a + $b;
echo "<br>";
$a = 345;
$b = 98;
echo ($a ** 2) + ($b ** 2);
echo "<br>";
$arr = [1, 7, 19];
$arr_count = count($arr);
echo array_sum($arr) / $arr_count;
echo "<br>";
$a = 8;
$b = 32;
$c = 16;
echo (++$a) - (2 * ($c - ($a * 2) + $b));
echo "<br>";
$x = 122;
echo $x % 3 . "<br>";
echo $x % 5 . "<br>";

$y = 48;
echo ($y * 0.3) + $y . "<br>";
echo ($y * 1.2) + $y . "<br>";
$q = 80;
$w = 150;
echo ($q * 0.4) + ($w * 0.85) . "<br>";

$a = 123;
$numArr = str_split($a);
echo array_sum($numArr) . "<br>";

$a = 123;
$a = (string)$a;
$a[1] = 0;
echo (integer)$a . "<br>";
$arrNum = array_reverse(str_split($a));
echo (integer)implode('', $arrNum) . "<br>";

$a = 575;
echo ($a % 2) == 0 ? 'четное число' : 'нечетное число';